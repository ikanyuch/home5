import java.util.*;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   private static final Map<String, String> RULES_SUBSTRING = new HashMap<>();
   // sümbolid mis peaks ekraneerida kahene slešiga, see on vaja sellepärast, et saada võrdlus regulaarselt
   private static final List<Character> CHARS_TO_ESCAPE = Arrays.asList(')', '(', ',');
   // regex expression eeskirjad, otsin märgud
   static {
      RULES_SUBSTRING.put("()", "Empty brackets");
      RULES_SUBSTRING.put(",)", "No node after comma");
      RULES_SUBSTRING.put("(,", "No node before comma");
      RULES_SUBSTRING.put(",,", "No node between commas");
      RULES_SUBSTRING.put(")[ \t(),]+", "Wrong node name");
      RULES_SUBSTRING.put("))", "No node name after closing bracket");
   }
// konstruktorid
   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   Node (String n) {
      this(n, null, null);
   }
   // esmalt kontrollin siis parsimit, kui viga on pakitud teise ekspeditsiooni ja väljastatakse vea string
   public static Node parsePostfix (String s) {
      try {
         checkInput(s);
         return parseNode(s);
      } catch(Exception e) {
         throw new RuntimeException(format("Input: %s, Error: %s", s, e.getMessage()));
      }
   }
// parseerin puu stringist
   private static Node parseNode(String s) {
      //muutajad
      String currentName = "";
      boolean isRoot = false;
// muutuv tase on vajalik selleks, et meeles pidada, millises pesitsustasandis oleme
      Integer level = 0;

      // ja mäletage iga taseme esimest ja viimast sõlme
      Map<Integer, Node> currentNodes = new HashMap<>();
      Map<Integer, Node> firstChildren = new HashMap<>();
// vaatan iga sümbol järjekorraga
      for(char c : s.toCharArray()) {
         // kui kronstein avab see on esimene taseme
         if(c == '(') {
            if(!currentName.isEmpty())
               throw new RuntimeException("Incorrect opening bracket");
            ++level;
         } else if(c == ')' || c == ',') {
            if(level == 0)
               throw new RuntimeException("No opening bracket or extra comma");
// looge sõlm, mille nimi on varem lugenud (võib-olla mitu märki) ja lähtestada
            Node newNode = new Node(currentName);
            currentName = "";
// kui sulgud suleti, peaks see sõlm muutuma uueks juureks ja paneme oma esimese lapse eelmisest tasemest
            if(isRoot) {
               newNode.firstChild = firstChildren.get(level + 1);
               firstChildren.remove(level + 1);
               currentNodes.remove(level + 1);
            }
// kui sellel tasemel pole veel sõlme, lisage see esimene
            if(!firstChildren.containsKey(level)) {
               firstChildren.put(level, newNode);
            } else {
               // kui oleks, siis paneme sellele viimase tasandi lingi sellel tasemel
               currentNodes.get(level).nextSibling = newNode;
            }
            // panen selle sõlme oma tasemele
            currentNodes.put(level, newNode);
            // panen Foagi, et lapse sõlmed on möödas ja on vaja teha uus, kui lõpetan järgmise nime lugemise
            if(isRoot = c == ')') {
               // muutan taseme
               --level;
            }
         } else {
            if(c == ' ') {
               throw new RuntimeException("Whitespace in node name");
            }
            currentName += c;
         }
      }
      // tagastage see puu (juur), mille nimi on viimane loetud ja esimene laps eelmisest tasemest
      return new Node(currentName, firstChildren.get(level + 1), null);
   }
// kontrollige sisendit regulaaravaldiste abil
   private static void checkInput(String s) {
      if(s.replaceAll("\\s", "").isEmpty()) {
        // kustuta kõik tühikud ja vaata, kas tühi siis obshibka
         throw new RuntimeException("Empty string");
      }
      if(s.contains(",") && !s.contains("(")) {
        // kui on pilk, kuid ei ole avatud sulgusid, siis viga (kuna koma peab olema sulgudes)
         throw new RuntimeException("No root");
      }
      String regex;
      for(Map.Entry<String, String> entry : RULES_SUBSTRING.entrySet()) {

         // vaadake kõiki reegleid iga väljenduse kohta ja kontrollige selle stringi
         regex = buildRegex(entry.getKey());
         if(Pattern.matches(regex, s)) {
            throw new RuntimeException(entry.getValue());
         }
      }
   }

   // loob reegli väljenduse
   private static String buildRegex(String rule) {
      StringBuilder regex = new StringBuilder();
      boolean ignore = false;
      // vaadake iga märki ja kui see tuleb välja pääseda, lisan selle ees kahekordse kaldkriipsuga
      for(char c : rule.toCharArray()) {
         if(!ignore && CHARS_TO_ESCAPE.contains(c)) {
           // kui need tähed nurksulgudes ei ole kaldkriipsuga vajalikud, sest seal ilma nendeta töötavad
            regex.append("\\");
         } else if(c == '[') {
            ignore = true;
         } else if(c == ']') {
           // kui need sulgud lõpevad, siis on see vajalik
            ignore = false;
         }
         regex.append(c);
      }
      // ümbritseb tärniga, mis tähendab, et enne ja pärast seda, kui on midagi, see tähendab, et me otsime vasteid stringide mis tahes osas
      return format(".*%s.*", regex.toString());
   }
   // stringide esitus
   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      // kirjutab nimi
      sb.append(name);
      // kui on lapsed siis kirjutab nende
      if (firstChild != null) {
         sb.append(format("(%s)", firstChild.leftParentheticRepresentation()));
         // kui on naaber siis kirjutab tema
      }
      if (nextSibling != null) {
         sb.append(format(",%s", nextSibling.leftParentheticRepresentation()));
      }

      // nad teevad ülejäänud rekursiivselt ise ja trükitakse kogu puu
      return sb.toString();
   }

   public static void main (String[] param) {
       String s = "(B1,C)A";
      //String s = "((B)C(D)E)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}